# sso-poc

PoC for implementing Google Single Sign on using Open ID Connect.

# Steps to run the PoC on a local server. 

Prerequisites: 
1. docker : https://docs.docker.com/install/
2. docker-compose : https://docs.docker.com/compose/install/


1. Clone the repo to your local directory: https://gitlab.com/vlead-systems/sso-poc
2. After cloning the repo, we need to create 3 files in the rp/ directory. 
	2.1 oidc.env (oidc.env)
	2.2 oidc.pwd (touch oidc.pwd)
	2.3 cert/ (mkdir cert)
